package com.kolyadruz.mycity.di;

import android.content.Context;

import com.kolyadruz.mycity.di.modules.ContextModule;
import com.kolyadruz.mycity.di.modules.LocalNavigationModule;
import com.kolyadruz.mycity.di.modules.NavigationModule;
import com.kolyadruz.mycity.di.modules.NetworkModule;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.mvp.presenters.ArticleDetailPresenter;
import com.kolyadruz.mycity.mvp.presenters.AutoLoginPresenter;
import com.kolyadruz.mycity.mvp.presenters.FavoritesPresenter;
import com.kolyadruz.mycity.mvp.presenters.HomePagePresenter;
import com.kolyadruz.mycity.mvp.presenters.PostDetailPresenter;
import com.kolyadruz.mycity.mvp.presenters.PostsListPresenter;
import com.kolyadruz.mycity.mvp.presenters.SearchPresenter;
import com.kolyadruz.mycity.ui.activities.ArticleDetail;
import com.kolyadruz.mycity.ui.activities.AutoLogin;
import com.kolyadruz.mycity.ui.activities.PostDetail;
import com.kolyadruz.mycity.ui.fragments.favorites.Favorites;
import com.kolyadruz.mycity.ui.fragments.home.HomePage;
import com.kolyadruz.mycity.ui.fragments.home.banners.Banner;
import com.kolyadruz.mycity.ui.fragments.home.categoriesANDarticles.CategoriesPager;
import com.kolyadruz.mycity.ui.fragments.qrscanner.QrScanner;
import com.kolyadruz.mycity.ui.fragments.search.Search;
import com.kolyadruz.mycity.ui.activities.MainActivity;
import com.kolyadruz.mycity.ui.fragments.home.categoriesANDarticles.PostsList;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ContextModule.class, NetworkModule.class, NavigationModule.class, LocalNavigationModule.class})

public interface AppComponent {

    Context getContext();
    NetworkService getNetwotkService();

    void inject(AutoLoginPresenter autoLoginPresenter);
    void inject(HomePagePresenter homePresenter);
    void inject(FavoritesPresenter favoritesPresenter);
    void inject(PostsListPresenter postsListPresenter);
    void inject(SearchPresenter searchPresenter);
    void inject(PostDetailPresenter postDetailPresenter);
    void inject(ArticleDetailPresenter articleDetailPresenter);

    void inject(AutoLogin autologin);
    void inject(MainActivity mainActivity);
    void inject(Search search);
    void inject(HomePage homePage);
    void inject(Banner banner);
    void inject(CategoriesPager categoriesPager);
    void inject(PostsList postsList);
    void inject(Favorites favorites);
    void inject(PostDetail postDetail);
    void inject(ArticleDetail articleDetail);
    void inject(QrScanner qrScanner);

}
