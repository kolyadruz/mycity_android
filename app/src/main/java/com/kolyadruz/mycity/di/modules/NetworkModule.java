package com.kolyadruz.mycity.di.modules;

import com.kolyadruz.mycity.app.Api;
import com.kolyadruz.mycity.mvp.NetworkService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class})
public class NetworkModule {

    @Provides
    @Singleton
    public NetworkService provideNetworkService(Api api) {
        return new NetworkService(api);
    }

}
