package com.kolyadruz.mycity.mvp.presenters;

import android.content.Intent;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.ContactsDetail;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.LoginResponseData;
import com.kolyadruz.mycity.mvp.views.CollabarationView;
import com.kolyadruz.mycity.utils.IntentUtils;
import com.kolyadruz.mycity.utils.PrefUtils;

@InjectViewState
public class CollabarationPresenter extends BasePresenter<CollabarationView> {

    private ContactsDetail contacts;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        contacts = new Gson().fromJson(PrefUtils.getPrefs().getString("mainData", ""), LoginResponseData.class).data.contacts;

        loadData();
    }

    private void loadData() {

        getViewState().showInfo(contacts.phoneCall, contacts.email, contacts.emailTech);

    }

    public void onEmailPressed() {

        Intent intent = IntentUtils.getEmailIntent(contacts.email, "Заявка", "Здравствуйте! Хочу разместить рекламу в вашем приложении.");

        if (intent != null) {
            getViewState().startIntent(intent);
        }
    }

    public void onEmailTechPressed() {

        Intent intent = IntentUtils.getEmailIntent(contacts.email, "Техпомощь", " ");

        if (intent != null) {
            getViewState().startIntent(intent);
        }
    }

    public void callPressed() {

        Intent intent = IntentUtils.getCallIntent(contacts.phoneCall);

        if (intent != null) {
            getViewState().startIntent(intent);
        }
    }

    public void onInstaPressed() {

        Intent intent = IntentUtils.getInstagramIntent(contacts.instagram);

        if (intent != null) {
            getViewState().startIntent(intent);
        }
    }

    public void onWaPressed() {

        Intent intent = IntentUtils.getWhatsAppIntent(contacts.phoneWa, "Здравствуйте! Хочу разместить рекламу в вашем приложении.");

        if (intent != null) {
            getViewState().startIntent(intent);
        }
    }

}
