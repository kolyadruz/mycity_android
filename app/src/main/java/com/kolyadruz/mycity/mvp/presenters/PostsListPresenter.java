package com.kolyadruz.mycity.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.utils.RxUtils;
import com.kolyadruz.mycity.mvp.models.RequestBodies.CategoryRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Category.CategoryResponseData;
import com.kolyadruz.mycity.mvp.views.PostsListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class PostsListPresenter extends BasePresenter<PostsListView> {

    private static final String TAG = "PostsListPresenter";

    @Inject
    NetworkService networkService;

    private Router localRouter;
    private int categoryId;

    private List<PublicationDetail> posts = new ArrayList<>();

    public PostsListPresenter(Router localRouter, int categoryId) {

        this.categoryId = categoryId;
        this.localRouter = localRouter;

    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        App.getAppComponent().inject(this);
        loadData();

    }

    private void loadData() {

        String token = PrefUtils.getPrefs().getString("token", "");

        CategoryRequestBody requestBody = new CategoryRequestBody();
        requestBody.categoryId = this.categoryId;

        Log.d(TAG, "requestBody: " + new Gson().toJson(requestBody));

        Subscription subscription =  networkService.category("Bearer " + token, requestBody)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<CategoryResponseData>() {
                    @Override
                    public void onCompleted() {
                        getViewState().hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.getMessage());

                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(CategoryResponseData categoryResponseData) {

                        if (categoryResponseData.data.err_id == 0) {

                            posts.clear();

                            posts.addAll(categoryResponseData.data.fixed);
                            posts.addAll(categoryResponseData.data.posts);

                            getViewState().updateList(categoryResponseData.data.fixed, categoryResponseData.data.posts);

                        } else {

                            getViewState().showErrorID(categoryResponseData.data.err_id);

                        }
                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    public void onPostClicked(int position) {

        int postId = posts.get(position).id;
        localRouter.navigateTo(new Screens.PostDetailScreen(postId));

    }

}
