package com.kolyadruz.mycity.mvp;

import com.kolyadruz.mycity.mvp.models.RequestBodies.ArticleRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.ArticleToFavRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.CategoryRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.FavoritesRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.LoginRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.PostRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.PostToFavRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.ReadQrRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.SearchRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.ArticleToFav.ArticleToFavResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Articles.ArticleDetailResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Category.CategoryResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Favorites.FavoritesResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.LoginResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.MainUpdateDataResponse;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PostToFav.PostToFavResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.PostDetailResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.QR.QRResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Search.SearchResponseData;
import com.kolyadruz.mycity.app.Api;

import retrofit2.Call;
import rx.Observable;

public class NetworkService {

    private Api api;

    public NetworkService(Api api) {
        this.api = api;
    }

    public Observable<LoginResponseData> login(String token, LoginRequestBody requestBody) {
        return api.login(token, requestBody);
    }

    public Observable<SearchResponseData> search(String token, SearchRequestBody requestBody) {
        return api.search(token, requestBody);
    }

    public Observable<CategoryResponseData> category(String token, CategoryRequestBody requestBody) {
        return api.category(token, requestBody);
    }

    public Observable<PostDetailResponseData> getpostdetail(String token, PostRequestBody requestBody) {
        return api.getpostdetail(token, requestBody);
    }

    public Observable<FavoritesResponseData> getfavorites(String token, FavoritesRequestBody requestBody) {
        return api.getfavorites(token, requestBody);
    }

    public Observable<PostToFavResponseData> posttofav(String token, PostToFavRequestBody requestBody) {
        return api.posttofav(token, requestBody);
    }

    public Observable<PostToFavResponseData> postremfav(String token, PostToFavRequestBody requestBody) {
        return api.postremfav(token, requestBody);
    }

    public Observable<ArticleDetailResponseData> getarticle(String token, ArticleRequestBody requestBody) {
        return api.getarticle(token, requestBody);
    }

    public Observable<ArticleToFavResponseData> articletofav(String token, ArticleToFavRequestBody requestBody) {
        return api.articletofav(token, requestBody);
    }

    public Observable<ArticleToFavResponseData> articleremfav(String token, ArticleToFavRequestBody requestBody) {
        return api.articleremfav(token, requestBody);
    }

    public Call<QRResponseData> sendqr(ReadQrRequestBody requestBody) {
        return api.sendqr(requestBody);
    }

    public Observable<MainUpdateDataResponse> getmaindata(String token) {
        return api.getmaindata(token);
    }


}
