package com.kolyadruz.mycity.mvp.models.ResponseModels.Login;

import com.google.gson.annotations.SerializedName;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.io.Serializable;
import java.util.List;

public class MainData implements Serializable {

    @SerializedName("token")
    public String token;

    @SerializedName("err_id")
    public Integer err_id;

    @SerializedName("banners")
    public List<PublicationDetail> banners;

    @SerializedName("categories")
    public List<CategoryDetail> categories;

    @SerializedName("articles")
    public List<PublicationDetail> articles;

    @SerializedName("contacts")
    public ContactsDetail contacts;

}
