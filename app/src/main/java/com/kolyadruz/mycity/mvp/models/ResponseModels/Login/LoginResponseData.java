package com.kolyadruz.mycity.mvp.models.ResponseModels.Login;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponseData implements Serializable {

    @SerializedName("data")
    public MainData data;

}
