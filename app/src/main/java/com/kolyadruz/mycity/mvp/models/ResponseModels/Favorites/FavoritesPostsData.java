package com.kolyadruz.mycity.mvp.models.ResponseModels.Favorites;

import com.google.gson.annotations.SerializedName;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

public class FavoritesPostsData {

    @SerializedName("err_id")
    public Integer err_id;

    @SerializedName("data")
    public List<PublicationDetail> data;

}
