package com.kolyadruz.mycity.mvp.views;

import android.content.Intent;

import com.kolyadruz.mycity.mvp.models.ResponseModels.Articles.ArticleDetailExtra;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;

import java.util.List;

public interface ArticleDetailView extends BaseMvpView {

    void showInfo(ArticleDetailExtra post);

    void buildAlert(List<Phone> phoneNumbers);

    void setLiked(boolean isFavorite);

    void startIntent(Intent intent);

}
