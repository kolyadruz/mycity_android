package com.kolyadruz.mycity.mvp.models.ResponseModels.Posts;

import com.google.gson.annotations.SerializedName;

public class PostDetailData {

    @SerializedName("err_id")
    public Integer err_id;

    @SerializedName("postData")
    public PublicationDetailExtra postData;

}
