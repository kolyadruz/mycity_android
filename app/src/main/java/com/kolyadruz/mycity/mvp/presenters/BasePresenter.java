package com.kolyadruz.mycity.mvp.presenters;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.kolyadruz.mycity.utils.ErrorMessageUtils;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class BasePresenter<View extends MvpView> extends MvpPresenter<View> {
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    protected void unsubscribeOnDestroy(@NonNull Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        compositeSubscription.clear();
    }

    protected String getErrorMessage(int err_id) {
        return ErrorMessageUtils.getErrorMessage(err_id);
    }

}
