package com.kolyadruz.mycity.mvp.models.ResponseModels.Posts;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PublicationDetailExtra {

    @SerializedName("id")
    public Integer id;
    @SerializedName("views")
    public Integer views;
    @SerializedName("isFavorite")
    public boolean isFavorite;
    @SerializedName("datePublication")
    public String datePublication;
    @SerializedName("image")
    public String image;
    @SerializedName("title")
    public String title;
    @SerializedName("body")
    public String body;
    @SerializedName("instagram")
    public String instagram;
    @SerializedName("phoneWa")
    public String phoneWa;
    @SerializedName("phonesCall")
    public List<Phone> phonesCall;
    @Nullable
    @SerializedName("qrData")
    public QRdetail qrData;

}
