package com.kolyadruz.mycity.mvp.models.ResponseModels.Login;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContactsDetail implements Serializable {

    @SerializedName("email")
    public String email;

    @SerializedName("emailtech")
    public String emailTech;

    @SerializedName("instagram")
    public String instagram;

    @SerializedName("phoneCall")
    public String phoneCall;

    @SerializedName("phoneWa")
    public String phoneWa;

}
