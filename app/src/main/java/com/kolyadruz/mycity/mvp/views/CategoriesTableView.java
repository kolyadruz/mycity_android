package com.kolyadruz.mycity.mvp.views;

import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.CategoryDetail;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

public interface CategoriesTableView extends BaseMvpView {

    void updateCategories(List<CategoryDetail> categories);
    void updateArticles(List<PublicationDetail> articles);

}
