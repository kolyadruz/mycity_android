package com.kolyadruz.mycity.mvp.models.ResponseModels.Login;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoryDetail implements Serializable {

    @SerializedName("id")
    public Integer id;

    @SerializedName("image")
    public String image;

    @SerializedName("name")
    public String name;

    @SerializedName("notViewed")
    public Integer notViewed;

}
