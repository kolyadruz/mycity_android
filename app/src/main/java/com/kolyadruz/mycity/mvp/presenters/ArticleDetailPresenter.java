package com.kolyadruz.mycity.mvp.presenters;

import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.util.Log;

import androidx.core.text.HtmlCompat;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.mvp.models.RequestBodies.ArticleRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.ArticleToFavRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.ArticleToFav.ArticleToFavResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Articles.ArticleDetailExtra;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Articles.ArticleDetailResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;
import com.kolyadruz.mycity.mvp.views.ArticleDetailView;
import com.kolyadruz.mycity.utils.IntentUtils;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.utils.RxUtils;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class ArticleDetailPresenter extends BasePresenter<ArticleDetailView>{

    private static final String TAG = "ArticleDetailPresenter";

    @Inject
    NetworkService networkService;

    private Router router;
    private int articleId;

    private ArticleDetailExtra currentArticle;

    public ArticleDetailPresenter(Router router, int articleId) {
        this.router = router;
        this.articleId = articleId;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        App.getAppComponent().inject(this);

        loadData();
    }

    private void loadData() {

        String token = PrefUtils.getPrefs().getString("token", "");

        ArticleRequestBody body = new ArticleRequestBody();
        body.articleId = articleId;

        Subscription subscription = networkService.getarticle("Bearer " + token, body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<ArticleDetailResponseData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.toString());
                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(ArticleDetailResponseData articleDetailResponseData) {

                        Log.d(TAG, "onNext: " + (new Gson()).toJson(articleDetailResponseData));

                        if (articleDetailResponseData.data.err_id == 0) {

                            currentArticle = articleDetailResponseData.data.articleData;

                            getViewState().showInfo(currentArticle);
                            getViewState().buildAlert(currentArticle.phonesCall);

                        } else {

                            getViewState().showErrorID(articleDetailResponseData.data.err_id);

                        }
                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    public void onCallPressed(int position) {

        Intent intent = IntentUtils.getCallIntent(currentArticle.phonesCall.get(position).phone);

        if (intent != null) {
            startIntent(intent);
        }

    }


    public void onClosePressed() {

        router.exit();

    }

    public void onLikeTapped() {

        String token = PrefUtils.getPrefs().getString("token", "");

        ArticleToFavRequestBody body = new ArticleToFavRequestBody();
        body.articleId = articleId;

        Subscription subscription = (currentArticle.isFavorite? networkService.articleremfav(token, body) : networkService.articletofav(token, body))
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<ArticleToFavResponseData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(ArticleToFavResponseData articleToFavResponseData) {

                        if(articleToFavResponseData.data.err_id == 0) {

                            currentArticle.isFavorite = !currentArticle.isFavorite;
                            getViewState().setLiked(currentArticle.isFavorite);


                        } else {

                            getViewState().showErrorID(articleToFavResponseData.data.err_id);

                        }

                    }
                });

        unsubscribeOnDestroy(subscription);

    }


    public void waSharePressed() {

        String text = getSharingBodyText();

        Intent intent = IntentUtils.getWhatsAppIntent(null, text);

        if (intent != null) {
            startIntent(intent);
        }

    }

    public void otherSharePressed() {

        String text = getSharingBodyText();

        Intent intent = IntentUtils.getShareIntent(text);

        if (intent != null) {
            startIntent(Intent.createChooser(intent, "Поделиться постом"));
        }

    }

    private void startIntent(Intent intent) {

        getViewState().startIntent(intent);

    }

    private String getSharingBodyText() {

        StringBuilder stringBuilder = new StringBuilder(currentArticle.title+"<br />"+ currentArticle.body+"<br /><br />");

        if (currentArticle.phonesCall.size() > 0) {

            stringBuilder.append("Контактные телефоны:<br />");

            for (Phone phone: currentArticle.phonesCall) {
                stringBuilder.append(phone.phone);
                stringBuilder.append("<br />");
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(stringBuilder.toString(), Html.FROM_HTML_MODE_COMPACT).toString();
        } else {
            return HtmlCompat.fromHtml(stringBuilder.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY).toString();
        }

    }

}
