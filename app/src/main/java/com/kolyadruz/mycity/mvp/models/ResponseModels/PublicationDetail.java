package com.kolyadruz.mycity.mvp.models.ResponseModels;

import com.google.gson.annotations.SerializedName;

public class PublicationDetail {

    @SerializedName("id")
    public Integer id;
    @SerializedName("image")
    public String image;
    @SerializedName("titles")
    public String title;
    @SerializedName("views")
    public Integer views;
    @SerializedName("datePublication")
    public String datePublication;
    @SerializedName("type")
    public int type;

}
