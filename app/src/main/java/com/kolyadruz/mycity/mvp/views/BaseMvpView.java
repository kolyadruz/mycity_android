package com.kolyadruz.mycity.mvp.views;

import com.arellomobile.mvp.MvpView;

public interface BaseMvpView extends MvpView {

    void showErrorID(int errID);
    void showConnectionErrorWithRequestCode(int requestCode);

}
