package com.kolyadruz.mycity.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.LoginResponseData;
import com.kolyadruz.mycity.mvp.views.HomeView;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class HomePagePresenter extends BasePresenter<HomeView> {

    @Inject
    NetworkService networkService;

    private Router router;

    public HomePagePresenter(Router router) {
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        App.getAppComponent().inject(this);

        updateBanners();
    }

    private void updateBanners() {

        LoginResponseData mainData = (new Gson()).fromJson(PrefUtils.getPrefs().getString("mainData", ""), LoginResponseData.class);

        getViewState().setBanners(mainData.data.banners);

    }

    public void onBackPressed() {
        router.exit();
    }
}
