package com.kolyadruz.mycity.mvp.views;

import android.content.Intent;

public interface AboutView extends BaseMvpView {

    void showInfo(String version, String emailSupport);

    void startIntent(Intent intent);

}
