package com.kolyadruz.mycity.mvp.models.ResponseModels.Category;

import com.google.gson.annotations.SerializedName;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

public class CategoryPostsData {

    @SerializedName("categoryName")
    public String categoryName;
    @SerializedName("err_id")
    public Integer err_id;
    @SerializedName("fixed")
    public List<PublicationDetail> fixed;
    @SerializedName("posts")
    public List<PublicationDetail> posts;

}
