package com.kolyadruz.mycity.mvp.models.ResponseModels.Posts;

import com.google.gson.annotations.SerializedName;

public class Phone {
    @SerializedName("description")
    public String description;
    @SerializedName("phone")
    public String phone;
}
