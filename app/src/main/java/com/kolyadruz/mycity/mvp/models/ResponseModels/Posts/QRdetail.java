package com.kolyadruz.mycity.mvp.models.ResponseModels.Posts;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class QRdetail {

    @Nullable
    @SerializedName("description")
    public String description;
    @Nullable
    @SerializedName("qrCode")
    public String qrCode;
}
