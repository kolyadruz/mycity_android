package com.kolyadruz.mycity.mvp.views;

import com.kolyadruz.mycity.ui.fragments.settings.Topics;

public interface SettingsView extends BaseMvpView{

    void updateTopics(Topics subscriptions);

}
