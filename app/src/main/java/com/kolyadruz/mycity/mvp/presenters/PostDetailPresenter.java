package com.kolyadruz.mycity.mvp.presenters;

import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.util.Log;

import androidx.core.text.HtmlCompat;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.mvp.models.RequestBodies.PostRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.PostToFavRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PostToFav.PostToFavResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.PostDetailResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.PublicationDetailExtra;
import com.kolyadruz.mycity.mvp.views.PostDetailView;
import com.kolyadruz.mycity.utils.IntentUtils;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.utils.RxUtils;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class PostDetailPresenter extends BasePresenter<PostDetailView>{

    private static final String TAG = "PostDetailPresenter";

    @Inject
    NetworkService networkService;

    private Router router;
    private int postId;

    private PublicationDetailExtra currentPost;

    public PostDetailPresenter(Router router, int postId) {
        this.router = router;
        this.postId = postId;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        App.getAppComponent().inject(this);

        loadData();
    }

    private void loadData() {

        String token = PrefUtils.getPrefs().getString("token", "");

        PostRequestBody body = new PostRequestBody();
        body.postId = postId;

        Subscription subscription = networkService.getpostdetail("Bearer " + token, body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<PostDetailResponseData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.toString());
                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(PostDetailResponseData postDetailResponseData) {

                        Log.d(TAG, "onNext: " + (new Gson()).toJson(postDetailResponseData));

                        if (postDetailResponseData.data.err_id == 0) {

                            currentPost = postDetailResponseData.data.postData;

                            getViewState().showInfo(currentPost);
                            getViewState().buildAlert(currentPost.phonesCall);

                        } else {

                            getViewState().showErrorID(postDetailResponseData.data.err_id);

                        }
                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    public void onInstaPressed() {

        Intent intent = IntentUtils.getInstagramIntent(currentPost.instagram);

        if (intent != null) {
            startIntent(intent);
        }

    }

    public void onCallPressed(int position) {

        Intent intent = IntentUtils.getCallIntent(currentPost.phonesCall.get(position).phone);

        if (intent != null) {
            startIntent(intent);
        }

    }

    public void onWaPressed() {

        Intent intent = IntentUtils.getWhatsAppIntent(currentPost.phoneWa,"Здравствуйте!");

        if (intent != null) {
            startIntent(intent);
        }

    }

    public void onClosePressed() {

        router.exit();

    }

    public void onLikeTapped() {

        String token = PrefUtils.getPrefs().getString("token", "");

        PostToFavRequestBody body = new PostToFavRequestBody();
        body.postId = postId;

        Subscription subscription = (currentPost.isFavorite? networkService.postremfav(token, body) : networkService.posttofav(token, body))
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<PostToFavResponseData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(PostToFavResponseData postToFavResponseData) {

                        if(postToFavResponseData.data.err_id == 0) {

                            currentPost.isFavorite = !currentPost.isFavorite;
                            getViewState().setLiked(currentPost.isFavorite);


                        } else {

                            getViewState().showErrorID(postToFavResponseData.data.err_id);

                        }

                    }
                });

        unsubscribeOnDestroy(subscription);

    }


    public void waSharePressed() {

        String text = getSharingBodyText();

        Intent intent = IntentUtils.getWhatsAppIntent(null, text);

        if (intent != null) {
            startIntent(intent);
        }

    }

    public void otherSharePressed() {

        String text = getSharingBodyText();

        Intent intent = IntentUtils.getShareIntent(text);

        if (intent != null) {
            startIntent(Intent.createChooser(intent, "Поделиться постом"));
        }

    }

    private void startIntent(Intent intent) {

        getViewState().startIntent(intent);

    }

    private String getSharingBodyText() {

        StringBuilder stringBuilder = new StringBuilder(currentPost.title+"<br />"+currentPost.body+"<br /><br />");

        if (currentPost.phonesCall.size() > 0) {

            stringBuilder.append("Контактные телефоны:<br />");

            for (Phone phone: currentPost.phonesCall) {
                stringBuilder.append(phone.phone);
                stringBuilder.append("<br />");
            }
        }

        if (currentPost.instagram != null) {
            stringBuilder.append("<br />Инстаграм: http//www.instagram.com/");
            stringBuilder.append(currentPost.instagram);
        }

        if (currentPost.phoneWa != null) {
            stringBuilder.append("<br />WhatsApp");
            stringBuilder.append(currentPost.phoneWa);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(stringBuilder.toString(), Html.FROM_HTML_MODE_COMPACT).toString();
        } else {
            return HtmlCompat.fromHtml(stringBuilder.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY).toString();
        }

    }

    public void qrButtonTapped() {

        getViewState().showQRdialog(currentPost.qrData.description, currentPost.qrData.qrCode);

    }


}
