package com.kolyadruz.mycity.mvp.models.ResponseModels.Search;

import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

public class SearchData {

    public Integer err_id;
    public List<PublicationDetail> data;
    public String string;

}
