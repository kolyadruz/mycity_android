package com.kolyadruz.mycity.mvp.models.ResponseModels.Posts;

import com.google.gson.annotations.SerializedName;

public class PostDetailResponseData {

    @SerializedName("data")
    public PostDetailData data;

}
