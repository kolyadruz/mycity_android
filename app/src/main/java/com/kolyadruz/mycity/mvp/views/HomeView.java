package com.kolyadruz.mycity.mvp.views;

import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

public interface HomeView extends BaseMvpView {

    void setBanners(List<PublicationDetail> banners);

}
