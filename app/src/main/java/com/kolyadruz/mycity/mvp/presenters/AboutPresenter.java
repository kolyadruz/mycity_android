package com.kolyadruz.mycity.mvp.presenters;

import android.content.Intent;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.mycity.BuildConfig;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.ContactsDetail;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.LoginResponseData;
import com.kolyadruz.mycity.mvp.views.AboutView;
import com.kolyadruz.mycity.utils.IntentUtils;
import com.kolyadruz.mycity.utils.PrefUtils;

@InjectViewState
public class AboutPresenter extends BasePresenter<AboutView> {

    private ContactsDetail contacts;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        contacts = new Gson().fromJson(PrefUtils.getPrefs().getString("mainData", ""), LoginResponseData.class).data.contacts;

        loadData();
    }

    private void loadData() {

        getViewState().showInfo("Версия " + BuildConfig.VERSION_NAME, contacts.emailTech);

    }

    public void onSupportPressed() {

        Intent emailIntent = IntentUtils.getEmailIntent(contacts.emailTech, "Техпомощь", "Требуется помощь");

        if (emailIntent != null) {
            getViewState().startIntent(Intent.createChooser(emailIntent, "Написать Email..."));
        }

    }

}
