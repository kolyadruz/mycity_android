package com.kolyadruz.mycity.mvp.views;

import android.content.Intent;

import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.PublicationDetailExtra;

import java.util.List;

public interface PostDetailView extends BaseMvpView {

    void showInfo(PublicationDetailExtra post);

    void buildAlert(List<Phone> phoneNumbers);

    void setLiked(boolean isFavorite);

    void startIntent(Intent intent);

    void showQRdialog(String title, String url);

}
