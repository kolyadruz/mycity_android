package com.kolyadruz.mycity.mvp.views;

import android.content.Intent;

public interface CollabarationView extends BaseMvpView {

    void showInfo(String phoneNumber, String email, String emailTech);

    void startIntent(Intent intent);
}
