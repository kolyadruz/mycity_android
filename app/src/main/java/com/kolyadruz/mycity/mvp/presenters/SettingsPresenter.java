package com.kolyadruz.mycity.mvp.presenters;

import android.os.Build;
import android.util.Log;
import android.widget.CompoundButton;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.kolyadruz.mycity.mvp.views.SettingsView;
import com.kolyadruz.mycity.ui.fragments.settings.Topic;
import com.kolyadruz.mycity.ui.fragments.settings.Topics;
import com.kolyadruz.mycity.utils.PrefUtils;

@InjectViewState
public class SettingsPresenter extends BasePresenter<SettingsView> {

    private static final String TAG = "SettingsPresenter";

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadData();
    }

    private void loadData() {

        Topics subscriptions = (new Gson()).fromJson(PrefUtils.getPrefs().getString("subscriptions", ""), Topics.class);
        getViewState().updateTopics(subscriptions);

    }

    public void onSettingsSwitchTapped(int position, CompoundButton checkBox) {

        String topicVersion = "androidold";
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            topicVersion = "android";
        } else {
            topicVersion = "android";
        }*/

        Log.d(TAG, "pos" + position + ": " + checkBox.isChecked());

        Topics subscriptions = (new Gson()).fromJson(PrefUtils.getPrefs().getString("subscriptions", ""), Topics.class);

        if (position == 0) {

            for (Topic topic : subscriptions.elements) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(topicVersion + topic.getId());
                topic.setChecked(!checkBox.isChecked());
            }

            subscriptions.isAllSubscribed = (checkBox.isChecked()) ? false : true;

        } else {

            Topic topic = subscriptions.elements.get(position - 1);

            FirebaseMessaging.getInstance().unsubscribeFromTopic(topicVersion + topic.getId());
            topic.setChecked(!checkBox.isChecked());

            subscriptions.isAllSubscribed = checkIsAllSubscribed(subscriptions);

        }

        String subscriptionsStr = (new Gson()).toJson(subscriptions);
        PrefUtils.getEditor().putString("subscriptions", subscriptionsStr).commit();

        getViewState().updateTopics(subscriptions);

    }

    private boolean checkIsAllSubscribed(Topics subscriptions) {

        for (Topic topic: subscriptions.elements) {
            if (!topic.isChecked) {
                return false;
            }
        }

        return true;

    }

}
