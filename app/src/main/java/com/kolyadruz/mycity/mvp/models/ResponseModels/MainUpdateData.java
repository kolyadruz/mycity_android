package com.kolyadruz.mycity.mvp.models.ResponseModels;

import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.CategoryDetail;

import java.util.List;

public class MainUpdateData {

    public Integer err_id;
    public List<PublicationDetail> banners;
    public List<CategoryDetail> categories;
    public List<PublicationDetail> articles;

}
