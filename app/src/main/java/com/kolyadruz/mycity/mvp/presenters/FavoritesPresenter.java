package com.kolyadruz.mycity.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.mvp.models.RequestBodies.FavoritesRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Favorites.FavoritesResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.mvp.views.FavoritesView;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.Screen;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class FavoritesPresenter extends BasePresenter<FavoritesView> {

    private static final String TAG = "FavoritesPresenter";

    private Router router;

    private List<PublicationDetail> favorites;

    @Inject
    NetworkService networkService;

    public FavoritesPresenter(Router router) {
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        App.getAppComponent().inject(this);

        loadData();
    }

    private void loadData() {

        String token = PrefUtils.getPrefs().getString("token", "");

        FavoritesRequestBody body = new FavoritesRequestBody();

        Subscription subscription = networkService.getfavorites("Bearer " + token, body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<FavoritesResponseData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showEmptyTxt();
                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(FavoritesResponseData favoritesResponseData) {

                        if (favoritesResponseData.data.err_id == 0) {

                            favorites = favoritesResponseData.data.data;

                            getViewState().hideEmptyTx();

                            if (favorites.size() > 0) {
                                getViewState().updatePosts(favorites);
                                return;
                            }

                            getViewState().showEmptyTxt();

                        } else {

                            getViewState().showEmptyTxt();
                            getViewState().showErrorID(favoritesResponseData.data.err_id);

                        }

                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    public void onPostCLicked(int position) {

        PublicationDetail post = favorites.get(position);

        Screen screen = (post.type == 1 ? new Screens.PostDetailScreen(post.id) : new Screens.ArticleDetailScreen(post.id));

        router.navigateTo(screen);

    }

}
