package com.kolyadruz.mycity.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.LoginResponseData;
import com.kolyadruz.mycity.mvp.views.CategoriesTableView;

import java.util.List;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class CategoriesPagerPresenter extends BasePresenter<CategoriesTableView> {

    private static final String TAG = "CategoriesPagerPresenter";

    private Router router;

    private List<PublicationDetail> articles;

    public CategoriesPagerPresenter(Router router) {

        this.router = router;

    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        LoginResponseData mainData = (new Gson()).fromJson(PrefUtils.getPrefs().getString("mainData", ""), LoginResponseData.class);

        getViewState().updateCategories(mainData.data.categories);

        articles = mainData.data.articles;
        getViewState().updateArticles(articles);

    }

    public String onCategoryClicked(int position) {

        String mainDataStr = PrefUtils.getPrefs().getString("mainData", "");

        Gson gson = new Gson();
        LoginResponseData mainData = gson.fromJson(mainDataStr, LoginResponseData.class);

        int categoryId = mainData.data.categories.get(position).id;
        String title = mainData.data.categories.get(position).name;

        router.navigateTo(new Screens.PostsListScreen(categoryId));

        return(title);

    }

    public void openCategory(int categoryId) {

        router.navigateTo(new Screens.PostsListScreen(categoryId));

    }

    public void onArticleClicked(int position) {

        int articleId = articles.get(position).id;
        router.navigateTo(new Screens.ArticleDetailScreen(articleId));

    }

}
