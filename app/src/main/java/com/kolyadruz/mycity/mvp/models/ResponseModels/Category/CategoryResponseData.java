package com.kolyadruz.mycity.mvp.models.ResponseModels.Category;

import com.google.gson.annotations.SerializedName;

public class CategoryResponseData {

    @SerializedName("data")
    public CategoryPostsData data;

}
