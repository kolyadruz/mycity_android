package com.kolyadruz.mycity.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.utils.RxUtils;
import com.kolyadruz.mycity.mvp.models.RequestBodies.SearchRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Search.SearchResponseData;
import com.kolyadruz.mycity.mvp.views.SearchMvpView;

import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.Screen;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class SearchPresenter extends BasePresenter<SearchMvpView> {

    private static final String TAG = "SearchPresenter";
    
    private Router router;

    private List<PublicationDetail> posts;

    @Inject
    NetworkService networkService;

    public SearchPresenter(Router router) {
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        App.getAppComponent().inject(this);
    }

    public void onBackPressed() {
        router.exit();
    }

    public void SearchRequest(String text) {

        String token = PrefUtils.getPrefs().getString("token", "");

        SearchRequestBody body = new SearchRequestBody();
        body.string = text;

        Subscription subscription = networkService.search("Bearer " + token, body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<SearchResponseData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(SearchResponseData searchResponseData) {

                        if (searchResponseData.data.err_id == 0) {

                            posts = searchResponseData.data.data;

                            getViewState().updatePosts(posts);

                        } else {
                            getViewState().showErrorID(searchResponseData.data.err_id);
                        }

                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    public void onPostClicked(int position) {

        PublicationDetail post = posts.get(position);

        Screen screen = post.type == 1? new Screens.PostDetailScreen(post.id) : new Screens.ArticleDetailScreen(post.id);

        router.navigateTo(screen);

    }


}
