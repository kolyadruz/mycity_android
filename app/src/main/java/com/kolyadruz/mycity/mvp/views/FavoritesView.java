package com.kolyadruz.mycity.mvp.views;

import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

public interface FavoritesView extends BaseMvpView {

    void updatePosts(List<PublicationDetail> posts);

    void showEmptyTxt();
    void hideEmptyTx();

}
