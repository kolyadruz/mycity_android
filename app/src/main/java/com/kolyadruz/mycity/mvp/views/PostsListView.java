package com.kolyadruz.mycity.mvp.views;

import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

public interface PostsListView extends BaseMvpView {

    void updateList(List<PublicationDetail> fixed, List<PublicationDetail> posts);

    void showProgress();
    void hideProgress();

}
