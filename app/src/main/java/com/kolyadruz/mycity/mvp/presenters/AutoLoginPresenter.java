package com.kolyadruz.mycity.mvp.presenters;

import android.os.Build;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.ui.fragments.settings.Topic;
import com.kolyadruz.mycity.ui.fragments.settings.Topics;
import com.kolyadruz.mycity.mvp.models.RequestBodies.LoginRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.CategoryDetail;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.LoginResponseData;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.utils.PrefUtils;
import com.kolyadruz.mycity.utils.RxUtils;
import com.kolyadruz.mycity.mvp.views.AutoLoginView;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import rx.Observer;
import rx.Subscription;

@InjectViewState
public class AutoLoginPresenter extends BasePresenter<AutoLoginView> {

    private static final String TAG = "AutoLoginPresenter";

    private Router router;

    @Inject
    NetworkService networkService;

    public AutoLoginPresenter(Router router) {
        this.router = router;
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void permissionGranted() {

        String token = PrefUtils.getPrefs().getString("token", "");

        LoginRequestBody body = new LoginRequestBody();
        body.fb_token = "";
        body.password = "";

        Subscription subscription = networkService.login("Bearer " + token, body)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Observer<LoginResponseData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.getMessage());

                        getViewState().showConnectionErrorWithRequestCode(0);
                    }

                    @Override
                    public void onNext(LoginResponseData loginResponseData) {
                        if (loginResponseData != null) {

                            if (loginResponseData.data.err_id == 0) {

                                Log.d(TAG, "data not null");

                                Log.d(TAG, "onNext: token=" + loginResponseData.data.token);

                                PrefUtils.getEditor().putString("token", loginResponseData.data.token).commit();

                                subscribeToTopics(loginResponseData.data.categories);

                                String mainDataStr = new Gson().toJson(loginResponseData);

                                PrefUtils.getEditor().putString("mainData", mainDataStr).commit();

                                router.replaceScreen(new Screens.MainActivityScreen());

                            } else {

                                Log.d(TAG, "season with errid");
                                getViewState().showErrorID(loginResponseData.data.err_id);

                            }

                        } else {
                            Log.d(TAG, "data null");
                        }
                    }
                });

        unsubscribeOnDestroy(subscription);

    }

    private void subscribeToTopics(List<CategoryDetail> categories) {

        Gson gson = new Gson();

        Topics allAvailableTopics = new Topics();

        Topics currentSubscriptions = gson.fromJson(PrefUtils.getPrefs().getString("subscriptions", ""), Topics.class);

        for (CategoryDetail categoryDetail: categories) {

            Topic topic = new Topic();
            topic.setName(categoryDetail.name);
            topic.setId(categoryDetail.id);
            topic.setChecked(true);

            allAvailableTopics.elements.add(topic);

        }

        Log.d(TAG, "subscribeToTopics: allTopics=" + gson.toJson(allAvailableTopics));

        if (currentSubscriptions != null) {

            for (Topic topic : currentSubscriptions.elements) {

                for(Iterator<Topic> it = allAvailableTopics.elements.iterator(); it.hasNext();) {
                    Topic t = it.next();
                    if(t.getId().equals(topic.getId())) {
                        it.remove();
                    }
                }

            }

            Log.d(TAG, "subscribeToTopics: Topics to subscribe=" + gson.toJson(allAvailableTopics));

            for (Topic topic: allAvailableTopics.elements) {

                FirebaseMessaging.getInstance().subscribeToTopic(getTopicNamePrefixByVersion() + topic.getId());
                currentSubscriptions.elements.add(topic);

            }

        } else {

            allAvailableTopics.isAllSubscribed = true;
            currentSubscriptions = allAvailableTopics;

        }

        for (Topic topic: currentSubscriptions.elements) {

            if (topic.isChecked) {
                FirebaseMessaging.getInstance().subscribeToTopic(getTopicNamePrefixByVersion() + topic.getId());
            }

        }

        Log.d(TAG, "subscribeToTopics: Final subscriptions="+gson.toJson(currentSubscriptions));

        PrefUtils.getEditor().putString("subscriptions", gson.toJson(currentSubscriptions)).commit();

    }

    private String getTopicNamePrefixByVersion() {

        return "androidold";

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            return "and";

        } else {

            return "andold";

        }*/

    }



}
