package com.kolyadruz.mycity.mvp.models.ResponseModels.Articles;

import com.google.gson.annotations.SerializedName;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;

import java.util.List;

public class ArticleDetailExtra {

    @SerializedName("id")
    public Integer id;
    @SerializedName("views")
    public Integer views;
    @SerializedName("isFavorite")
    public boolean isFavorite;
    @SerializedName("datePublication")
    public String datePublication;
    @SerializedName("image")
    public String image;
    @SerializedName("title")
    public String title;
    @SerializedName("body")
    public String body;
    @SerializedName("phonesCall")
    public List<Phone> phonesCall;

}
