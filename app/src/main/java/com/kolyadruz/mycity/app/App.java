package com.kolyadruz.mycity.app;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.di.AppComponent;
import com.kolyadruz.mycity.di.DaggerAppComponent;
import com.kolyadruz.mycity.di.modules.ContextModule;

public class App extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel(this);

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
    }

    @SuppressLint("NewApi")
    public  void createNotificationChannel(@NonNull Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = getString(R.string.default_notification_channel_id);
            if(manager.getNotificationChannel(channelId)==null) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        "Мой город",
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription("Пушки Мой город");
                manager.createNotificationChannel(channel);
            }

        }
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent appComponent) {
        sAppComponent = appComponent;
    }

}