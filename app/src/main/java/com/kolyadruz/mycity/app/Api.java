package com.kolyadruz.mycity.app;

import com.kolyadruz.mycity.mvp.models.RequestBodies.ArticleRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.ArticleToFavRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.CategoryRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.FavoritesRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.LoginRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.PostRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.PostToFavRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.ReadQrRequestBody;
import com.kolyadruz.mycity.mvp.models.RequestBodies.SearchRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.ArticleToFav.ArticleToFavResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Articles.ArticleDetailResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Category.CategoryResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Favorites.FavoritesResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.LoginResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.MainUpdateDataResponse;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PostToFav.PostToFavResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.PostDetailResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.QR.QRResponseData;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Search.SearchResponseData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

import rx.Observable;

public interface Api {

    @POST("login")
    Observable<LoginResponseData> login(@Header("Authorization") String bearerToken, @Body LoginRequestBody loginBody);

    @POST("search")
    Observable<SearchResponseData> search(@Header("Authorization") String bearerToken, @Body SearchRequestBody searchBody);

    @POST("category")
    Observable<CategoryResponseData> category(@Header("Authorization") String bearerToken, @Body CategoryRequestBody categoryBody);

    @POST("v2/getpost")
    Observable<PostDetailResponseData> getpostdetail(@Header("Authorization") String bearerToken, @Body PostRequestBody postBody);

    @POST("favorites")
    Observable<FavoritesResponseData> getfavorites(@Header("Authorization") String bearerToken, @Body FavoritesRequestBody favoritesBody);

    @POST("posttofav")
    Observable<PostToFavResponseData> posttofav(@Header("Authorization") String bearerToken, @Body PostToFavRequestBody postToFavBody);

    @POST("postremovefav")
    Observable<PostToFavResponseData> postremfav(@Header("Authorization") String bearerToken, @Body PostToFavRequestBody postRemoveFavBody);

    @POST("v2/getarticle")
    Observable<ArticleDetailResponseData> getarticle(@Header("Authorization") String bearerToken, @Body ArticleRequestBody articleBody);

    @POST("articletofav")
    Observable<ArticleToFavResponseData> articletofav(@Header("Authorization") String bearerToken, @Body ArticleToFavRequestBody articleToFavBody);

    @POST("articleremovefav")
    Observable<ArticleToFavResponseData> articleremfav(@Header("Authorization") String bearerToken, @Body ArticleToFavRequestBody articleRemoveFavBody);

    @POST("readqr")
    Call<QRResponseData> sendqr(@Body ReadQrRequestBody readQrBody);

    @POST("getmaindata")
    Observable<MainUpdateDataResponse> getmaindata(@Header("Authorization") String bearerToken);

}
