package com.kolyadruz.mycity.utils;

import android.os.Build;

public class TopicsPrefixByVersion {

    public static String getTopicNamePrefixByVersion() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            return "android";

        } else {

            return "androidold";

        }

    }

}
