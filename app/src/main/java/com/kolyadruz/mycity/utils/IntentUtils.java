package com.kolyadruz.mycity.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.widget.Toast;

import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;

import java.util.List;

public class IntentUtils {

    public static Intent getEmailIntent(String emailAddress, String subject, String textHolder) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", emailAddress, null));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailAddress);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);//"Заявка");
        emailIntent.putExtra(Intent.EXTRA_TEXT, textHolder);//"Здравствуйте! Хочу разместить рекламу в вашем приложении.");

        return Intent.createChooser(emailIntent, "Написать Email...");
    }

    public static Intent getCallIntent(String phoneNumber) {

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel: " + phoneNumber));

        return intent;
    }

    public static Intent getInstagramIntent(String account) {

        Uri uri = Uri.parse("http://instagram.com/_u/" + account);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setPackage("com.instagram.android");

        if (isIntentAvailable(App.getAppComponent().getContext(), intent)) {
            return intent;
        } else {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + account));
        }
    }

    public static Intent getWhatsAppIntent(String phoneNumber, String text) {

        try {
            //Intent intent = new Intent("android.intent.action.MAIN");
            //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            //intent.setAction(Intent.ACTION_SEND);

            Intent intent = new Intent(Intent.ACTION_SEND);

            intent.setType("text/plain");
            intent.setPackage("com.whatsapp");
            intent.putExtra(Intent.EXTRA_TEXT, text);

            if (phoneNumber != null) {
                intent.putExtra("wa", phoneNumber + "@s.whatsapp.net"); //phone number without "+" prefix
            }

            return intent;

        } catch (Exception e) {

            Toast.makeText(App.getAppComponent().getContext(), "WhatsApp не установлен", Toast.LENGTH_SHORT).show();
            return null;

        }

    }

    public static Intent getShareIntent(String text) {

        Intent intent = new Intent(android.content.Intent.ACTION_SEND);

        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Мой город");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, text);

        return intent;
    }


    private static boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

}
