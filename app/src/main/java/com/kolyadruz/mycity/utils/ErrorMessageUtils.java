package com.kolyadruz.mycity.utils;

public class ErrorMessageUtils {

    public static String getErrorMessage(int err_id) {

        switch (err_id) {
            case 1:
                return  "Не удалось выполнить операцию";
            case 2:
                return "Категория не найдена";
            case 3:
                return "Данный пост недоступен";
            case 4:
                return "Заполните все поля";
            case 5:
                return "Пользователь не найден";
            case  6:
                return "Запись недоступна";
            case 7:
                return "QR уже прочитан, либо недоступен";
            case 8:
                return "Приложение не установлено";
            default:
                return null;
        }

    }

}
