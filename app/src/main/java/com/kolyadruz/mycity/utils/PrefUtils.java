package com.kolyadruz.mycity.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.kolyadruz.mycity.app.App;

public class PrefUtils {
    private static final String PREF_NAME = "mycity";

    public static SharedPreferences getPrefs() {
        return App.getAppComponent().getContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }
}
