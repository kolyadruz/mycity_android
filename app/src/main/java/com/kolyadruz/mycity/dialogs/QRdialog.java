package com.kolyadruz.mycity.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kolyadruz.mycity.R;

import net.glxn.qrgen.android.QRCode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class QRdialog extends DialogFragment {

    @BindView(R.id.title)
    TextView titleTV;

    @BindView(R.id.image)
    ImageView imageView;

    private Context context;

    private Unbinder unbinder;

    private String url;
    private String title;

    public static QRdialog getNewInstance(String title, String url) {

        Bundle args = new Bundle();

        args.putString("title", title);
        args.putString("url", url);

        QRdialog fragment = new QRdialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString("title");
            url = getArguments().getString("url");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.qr_dialog, null);

        unbinder = ButterKnife.bind(this, view);

        titleTV.setText(title);

        Bitmap qrBitmap = QRCode.from(url).bitmap();
        imageView.setImageBitmap(qrBitmap);

        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setView(view);

        return adb.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }
}
