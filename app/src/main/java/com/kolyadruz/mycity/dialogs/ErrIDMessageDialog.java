package com.kolyadruz.mycity.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kolyadruz.mycity.utils.ErrorMessageUtils;

public class ErrIDMessageDialog extends DialogFragment {

    private static final String TAG_ERR_ID = "errID";

    private int errID;

    public static ErrIDMessageDialog getNewInstance(int errID) {

        Bundle args = new Bundle();

        args.putInt(TAG_ERR_ID, errID);

        ErrIDMessageDialog fragment = new ErrIDMessageDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            errID = getArguments().getInt(TAG_ERR_ID);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setMessage(ErrorMessageUtils.getErrorMessage(errID));
        adb.setCancelable(false);
        adb.setPositiveButton("ОК",(dialog, item) -> {
            this.dismiss();
        });

        return adb.create();
    }
}
