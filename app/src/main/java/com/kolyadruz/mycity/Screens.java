package com.kolyadruz.mycity;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.kolyadruz.mycity.ui.activities.ArticleDetail;
import com.kolyadruz.mycity.ui.activities.MainActivity;
import com.kolyadruz.mycity.ui.activities.PostDetail;
import com.kolyadruz.mycity.ui.fragments.about.About;
import com.kolyadruz.mycity.ui.fragments.home.categoriesANDarticles.CategoriesPager;
import com.kolyadruz.mycity.ui.fragments.collabaration.Collabaration;
import com.kolyadruz.mycity.ui.fragments.favorites.Favorites;
import com.kolyadruz.mycity.ui.fragments.home.HomePage;
import com.kolyadruz.mycity.ui.fragments.qrscanner.QrScanner;
import com.kolyadruz.mycity.ui.fragments.search.Search;
import com.kolyadruz.mycity.ui.fragments.settings.Settings;
import com.kolyadruz.mycity.ui.fragments.home.categoriesANDarticles.PostsList;

import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class MainActivityScreen extends SupportAppScreen {

        @Override
        public Intent getActivityIntent(Context context) {
            return new Intent(context, MainActivity.class);
        }
    }

    public static final class HomeScreen extends SupportAppScreen {

        int categoryId;

        public HomeScreen(int categoryId) {
            this.categoryId = categoryId;
        }

        @Override
        public Fragment getFragment() {
            return HomePage.getNewInstance(categoryId);
        }

    }


    public static final class FavoritesScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new Favorites();
        }

    }

    public static final class CollabarationScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new Collabaration();
        }

    }

    public static final class SettingsScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new Settings();
        }

    }

    public static final class QrScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new QrScanner();
        }

    }

    public static final class AboutScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new About();
        }

    }

    public static final class SearchScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new Search();
        }
    }

    public static final class CategoriesTableScreen extends SupportAppScreen {

        int categoryId;

        public CategoriesTableScreen(int categoryId){
            this.categoryId = categoryId;
        }

        @Override
        public Fragment getFragment() {
            return CategoriesPager.getNewInstance(categoryId);
        }
    }

    public static final class PostsListScreen extends SupportAppScreen {

        private int categoryId;

        public PostsListScreen(int categoryId) {
            this.categoryId = categoryId;
        }

        @Override
        public Fragment getFragment() {
            return PostsList.getNewInstance(categoryId);
        }
    }

    public static final class PostDetailScreen extends SupportAppScreen {

        private int postId;

        public PostDetailScreen(int postId) {

            this.postId = postId;

        }

        @Override
        public Intent getActivityIntent(Context context) {
            Intent intent = new Intent(context, PostDetail.class);

            intent.putExtra("postId", postId);

            return intent;
        }
    }

    public static final class ArticleDetailScreen extends SupportAppScreen {

        private int articleId;

        public ArticleDetailScreen(int articleId) {

            this.articleId = articleId;

        }

        @Override
        public Intent getActivityIntent(Context context) {
            Intent intent = new Intent(context, ArticleDetail.class);

            intent.putExtra("articleId", articleId);

            return intent;
        }
    }

}
