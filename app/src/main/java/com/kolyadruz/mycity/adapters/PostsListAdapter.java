package com.kolyadruz.mycity.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.vansuita.gaussianblur.GaussianBlur;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostsListAdapter extends RecyclerView.Adapter<PostsListAdapter.MyViewHolder> {

    private OnPostClickedListener mOnPostClickedListener;

    List<PublicationDetail> fixed = new ArrayList<>();
    List<PublicationDetail> posts = new ArrayList<>();

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.post_image)
        ImageView image;

        @BindView(R.id.post_blur_image)
        ImageView blurImage;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.viewsTxt)
        TextView views;

        OnPostClickedListener onPostClickedListener;

        public MyViewHolder(View v, OnPostClickedListener onPostClickedListener) {
            super(v);

            ButterKnife.bind(this, v);

            this.onPostClickedListener = onPostClickedListener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onPostClickedListener.onPostClicked(getAdapterPosition());
        }
    }

    public void updateList(List<PublicationDetail> fixed, List<PublicationDetail> posts) {
        this.fixed = fixed;
        this.posts = posts;
        notifyDataSetChanged();
    }

    public PostsListAdapter(OnPostClickedListener onPostClickedListener) {

        this.mOnPostClickedListener = onPostClickedListener;

    }

    @Override
    public PostsListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_list, parent, false);
        return new MyViewHolder(itemView, mOnPostClickedListener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        PublicationDetail publicationDetail = ((position < fixed.size())? fixed.get(position) : posts.get(position - fixed.size()));

        Glide.with(App.getAppComponent().getContext())
                .load(publicationDetail.image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        Bitmap blurredBitmap = GaussianBlur.with(App.getAppComponent().getContext()).size(200).radius(25).render(resource);
                        holder.blurImage.setImageBitmap(blurredBitmap);

                        return false;
                    }
                })
                .into(holder.image);

        holder.title.setText(publicationDetail.title);
        holder.views.setText("" + publicationDetail.views);

    }

    @Override
    public int getItemCount() {

        return fixed.size() + posts.size();

    }

    public void addData(List<PublicationDetail> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }

    public interface OnPostClickedListener {
        void onPostClicked(int position);
    }

}
