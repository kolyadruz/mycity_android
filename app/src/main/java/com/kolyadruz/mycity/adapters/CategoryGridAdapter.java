package com.kolyadruz.mycity.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.CategoryDetail;

import java.util.ArrayList;
import java.util.List;

public class CategoryGridAdapter extends RecyclerView.Adapter<CategoryGridAdapter.MyViewHolder> {

    List<CategoryDetail> categories = new ArrayList<>();

    OnCategoryItemClickListener mOnCategoryItemClickListener;

    public CategoryGridAdapter(OnCategoryItemClickListener onCategoryItemClickListener) {

        this.mOnCategoryItemClickListener = onCategoryItemClickListener;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category, viewGroup, false);

        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) itemView.getLayoutParams();
        lp.height = (viewGroup.getMeasuredHeight() - 40) / 3;
        lp.width = (viewGroup.getMeasuredWidth() - 80) / 4;

        itemView.setLayoutParams(lp);

        return new MyViewHolder(itemView, mOnCategoryItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        final CategoryDetail categoryDetail = categories.get(i);// + (pageNum * 12));

        Glide.with(App.getAppComponent().getContext()).load(categoryDetail.image).into(myViewHolder.icon);

        myViewHolder.title.setText(categoryDetail.name);

        if (categoryDetail.notViewed == 0) {
            myViewHolder.notViewedLayout.setVisibility(View.INVISIBLE);
        } else {
            myViewHolder.notViewed.setText(""+categoryDetail.notViewed);
        }

    }

    @Override
    public int getItemCount() {

        return categories.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView icon;
        TextView title;
        TextView notViewed;
        ConstraintLayout notViewedLayout;

        OnCategoryItemClickListener onCategoryItemClickListener;

        public MyViewHolder(@NonNull View itemView, OnCategoryItemClickListener onCategoryItemClickListener) {
            super(itemView);

            this.onCategoryItemClickListener = onCategoryItemClickListener;

            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);
            notViewed = itemView.findViewById(R.id.notViewedTxt);
            notViewedLayout = itemView.findViewById(R.id.notViewedLayout);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCategoryItemClickListener.onCategoryClicked(getAdapterPosition());
        }
    }

    public void updateCategories(List<CategoryDetail> categories) {

        this.categories = categories;
        notifyDataSetChanged();

    }

    public interface OnCategoryItemClickListener {
        void onCategoryClicked(int position);
    }

    public interface OnCategoryWithTitleClickListener {
        void onCategoryWithTitleClick(String title);
    }

}
