package com.kolyadruz.mycity.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.ui.fragments.home.banners.Banner;

import java.util.ArrayList;
import java.util.List;


public class TopBannersAdapter extends FragmentPagerAdapter {

    List<PublicationDetail> banners = new ArrayList<>();

    public TopBannersAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        PublicationDetail bannerDetail = banners.get(position);

        return Banner.getNewInstance(bannerDetail.image, bannerDetail.id);
    }

    @Override
    public int getCount() {
        if (banners == null) return 0;
        return banners.size();
    }

    public void updateBanners(List<PublicationDetail> banners) {
        this.banners = banners;
        notifyDataSetChanged();
    }
}