package com.kolyadruz.mycity.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.ui.fragments.settings.OnSettingsSwitchTappedListener;
import com.kolyadruz.mycity.ui.fragments.settings.Topic;
import com.kolyadruz.mycity.ui.fragments.settings.Topics;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsListAdapter  extends RecyclerView.Adapter<SettingsListAdapter.MyViewHolder>  {

    private OnSettingsSwitchTappedListener switchListener;
    private Topics subscriptions = new Topics();

    public SettingsListAdapter(OnSettingsSwitchTappedListener switchListener) {

        this.switchListener = switchListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_settings, parent, false);
        return new MyViewHolder(itemView, switchListener);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (position == 0) {

            holder.titleTxt.setText("Получать уведомления от всех");
            holder.checkBox.setChecked(subscriptions.isAllSubscribed);

        } else {

            Topic topic  = subscriptions.elements.get(position - 1);

            holder.titleTxt.setText(topic.getName());
            holder.checkBox.setChecked(topic.isChecked);

        }

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.settings_switch)
        CheckBox checkBox;

        @BindView(R.id.titleTxt)
        TextView titleTxt;

        private OnSettingsSwitchTappedListener switchListenerRef;

        public MyViewHolder(View itemView, OnSettingsSwitchTappedListener switchListener) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            switchListenerRef = switchListener;

            checkBox = itemView.findViewById(R.id.settings_switch);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switchListenerRef.onSettingsSwitchTapped(getAdapterPosition(), checkBox);

        }
    }

    @Override
    public int getItemCount() {

        return subscriptions.elements.size() + 1;

    }

    public void updateTopics(Topics subscriptions) {

        this.subscriptions = subscriptions;
        notifyDataSetChanged();

    }

}