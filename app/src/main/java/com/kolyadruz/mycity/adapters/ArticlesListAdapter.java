package com.kolyadruz.mycity.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Router;

public class ArticlesListAdapter extends RecyclerView.Adapter<ArticlesListAdapter.MyViewHolder> {

    private static final String TAG = "ArticlesListAdapter";

    @Inject
    Router router;

    List<PublicationDetail> articles = new ArrayList<>();

    OnArticleClickedListener onArticleClickedListener;

    public ArticlesListAdapter(OnArticleClickedListener onArticleClickedListener) {
        this.onArticleClickedListener = onArticleClickedListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_article, viewGroup, false);

        return new MyViewHolder(itemView, onArticleClickedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        final PublicationDetail articleDetail = articles.get(i);

        Glide.with(App.getAppComponent().getContext()).load(articleDetail.image).into(myViewHolder.article);

    }

    @Override
    public int getItemCount() {

        if (articles != null) {
            return articles.size();
        } else {
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnArticleClickedListener onArticleClickedListener;

        @BindView(R.id.article)
        ImageView article;

        public MyViewHolder(@NonNull View itemView, OnArticleClickedListener onArticleClickedListener) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            this.onArticleClickedListener = onArticleClickedListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: ");
            onArticleClickedListener.onArticleClicked(getAdapterPosition());
        }
    }

    public void updateArticles(List<PublicationDetail> articles) {
        this.articles = articles;
        notifyDataSetChanged();
    }

    public interface OnArticleClickedListener {
        void onArticleClicked(int position);
    }

}
