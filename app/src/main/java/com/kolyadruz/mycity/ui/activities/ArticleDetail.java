package com.kolyadruz.mycity.ui.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ConnectionErrorDialog;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Articles.ArticleDetailExtra;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;
import com.kolyadruz.mycity.mvp.presenters.ArticleDetailPresenter;
import com.kolyadruz.mycity.mvp.views.ArticleDetailView;
import com.vansuita.gaussianblur.GaussianBlur;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;

public class ArticleDetail extends MvpAppCompatActivity implements ArticleDetailView {

    @Inject
    Router router;

    @Inject
    NavigatorHolder navigatorHolder;

    private Navigator navigator = new SupportAppNavigator(this, -1);

    @InjectPresenter
    ArticleDetailPresenter presenter;

    @ProvidePresenter
    ArticleDetailPresenter provideArticleDetailPresenter() {
        return new ArticleDetailPresenter(router, getIntent().getIntExtra("articleId", 0));
    }

    AlertDialog.Builder phoneNumbersAlert;

    @BindView(R.id.like_btn)
    ImageButton likeBtn;

    @BindView(R.id.article_image)
    ImageView image;
    @BindView(R.id.article_blur_image)
    ImageView blurImage;

    @BindView(R.id.call_btn)
    FrameLayout callBtn;
    @BindView(R.id.call_btn_icon)
    ImageView callBtnIcon;
    @BindView(R.id.call_btn_title)
    TextView callBtnTitle;

    @BindView(R.id.article_title)
    TextView title;
    @BindView(R.id.article_views)
    TextView views;
    @BindView(R.id.description)
    TextView descriptionTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        App.getAppComponent().inject(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_article_detail);

        ButterKnife.bind(this);

    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @OnClick(R.id.call_btn)
    public void callTapped() {
        phoneNumbersAlert.show();
    }

    @OnClick(R.id.close_btn)
    public void closeTapped() {
        presenter.onClosePressed();
    }

    @OnClick(R.id.like_btn)
    public void likeTapped(){
        presenter.onLikeTapped();
    }

    @OnClick(R.id.wa_share_btn)
    public void waShareTapped() {
        presenter.waSharePressed();
    }

    @OnClick(R.id.other_share_button)
    public void otherShareTapped() {
        presenter.otherSharePressed();
    }

    @Override
    public void showInfo(ArticleDetailExtra article) {

        Glide.with(App.getAppComponent().getContext())
                .load(article.image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        Bitmap blurredBitmap = GaussianBlur.with(App.getAppComponent().getContext()).size(200).radius(25).render(resource);
                        blurImage.setImageBitmap(blurredBitmap);

                        return false;
                    }
                })
                .into(image);

        if (article.phonesCall.size() == 0) {
            callBtn.setEnabled(false);
            callBtnTitle.setTextColor(ContextCompat.getColor(this, R.color.light_gray));
            callBtnIcon.setColorFilter(ContextCompat.getColor(this, R.color.light_gray));
        }

        if (article.isFavorite) {
            likeBtn.setImageResource(R.drawable.ic_heart_active);
        }

        title.setText(article.title);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            descriptionTxt.setText(Html.fromHtml(article.body, Html.FROM_HTML_MODE_COMPACT));
        } else {
            descriptionTxt.setText(HtmlCompat.fromHtml(article.body, HtmlCompat.FROM_HTML_MODE_LEGACY));
        }

        views.setText(String.valueOf(article.views));

    }

    @Override
    public void buildAlert(List<Phone> phoneNumbers) {

        List<String> actions = new ArrayList<>();

        for (Phone phone: phoneNumbers) {
            actions.add(phone.description.equals("")? phone.phone : phone.description);
        }

        final String[] array = actions.toArray(new String[phoneNumbers.size()]);

        phoneNumbersAlert = new AlertDialog.Builder(this);
        phoneNumbersAlert.setTitle("Выберите номер для звонка");

        phoneNumbersAlert.setItems(array, (DialogInterface dialog, int position) -> {

            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},1);
            } else {
                presenter.onCallPressed(position);
            }

        });

    }

    @Override
    public void setLiked(boolean isFavorite) {
        likeBtn.setImageResource( isFavorite ? R.drawable.ic_heart_active : R.drawable.ic_heart);
    }

    @Override
    public void startIntent(Intent intent) {
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            showErrorID(8);
            ex.printStackTrace();
        }
    }

    @Override
    public void showErrorID(int errID)  {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getSupportFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {
        DialogFragment dialog = new ConnectionErrorDialog();
        dialog.startActivityForResult(new Intent(), requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



    }
}
