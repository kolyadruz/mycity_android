package com.kolyadruz.mycity.ui.fragments.home.categoriesANDarticles;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.material.tabs.TabLayout;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.adapters.ArticlesListAdapter;
import com.kolyadruz.mycity.adapters.CategoryGridAdapter;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.common.PagerSnapWithSpanCountHelper;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Login.CategoryDetail;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.mvp.presenters.CategoriesPagerPresenter;
import com.kolyadruz.mycity.mvp.views.CategoriesTableView;
import com.kolyadruz.mycity.ui.common.RouterProvider;
import com.kolyadruz.mycity.ui.fragments.home.HomePage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class CategoriesPager extends MvpAppCompatFragment implements CategoriesTableView, CategoryGridAdapter.OnCategoryItemClickListener, ArticlesListAdapter.OnArticleClickedListener {

    private static final int NUMBER_OF_COLUMNS = 3;

    //CATEGORIES
    @BindView(R.id.categoryPager)
    RecyclerView categoryPager;

    @BindView(R.id.categoryDots)
    TabLayout categoryDots;

    //ARTICLES
    @BindView(R.id.articles_recyclerView)
    RecyclerView articlesRecyclerView;
    private CategoryGridAdapter gridAdapter;

    private ArticlesListAdapter articlesAdapter;

    @InjectPresenter
    CategoriesPagerPresenter presenter;

    @ProvidePresenter
    CategoriesPagerPresenter provideCategoriesPresenter() {
        return new CategoriesPagerPresenter(((RouterProvider) getParentFragment()).getRouter());
    }

    private CategoryGridAdapter.OnCategoryWithTitleClickListener onCategoryWithTitleClickListener;

    private Unbinder unbinder;

    int categoryId = 0;

    public static CategoriesPager getNewInstance(int categoryId) {

        CategoriesPager fragment = new CategoriesPager();

        Bundle args = new Bundle();
        args.putInt("categoryId", categoryId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getInt("categoryId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_categories, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        setArticlesRecycler();
        setCategoriesRecycler();

        if (categoryId != 0) presenter.openCategory(categoryId);

        return rootView;
    }

    private void setArticlesRecycler() {

        LinearLayoutManager llm = new LinearLayoutManager(App.getAppComponent().getContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        articlesRecyclerView.setLayoutManager(llm);

        articlesAdapter = new ArticlesListAdapter(this);
        articlesRecyclerView.setAdapter(articlesAdapter);

    }

    private void setCategoriesRecycler() {

        PagerSnapWithSpanCountHelper snapHelper = new PagerSnapWithSpanCountHelper(4, true);
        snapHelper.attachToRecyclerView(categoryPager);

        GridLayoutManager layoutManager = new GridLayoutManager(App.getAppComponent().getContext(), NUMBER_OF_COLUMNS, LinearLayoutManager.HORIZONTAL, false);
        categoryPager.setLayoutManager(layoutManager);
        categoryPager.addItemDecoration(new SpacesItemDecoration(10));

        gridAdapter = new CategoryGridAdapter(this);
        categoryPager.setAdapter(gridAdapter);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCategoryClicked(int position) {
        onCategoryWithTitleClickListener.onCategoryWithTitleClick(presenter.onCategoryClicked(position));
    }

    @Override
    public void onArticleClicked(int position) {
        presenter.onArticleClicked(position);
    }

    @Override
    public void updateCategories(List<CategoryDetail> categories) {
        gridAdapter.updateCategories(categories);
    }

    @Override
    public void updateArticles(List<PublicationDetail> articles) {
        articlesAdapter.updateArticles(articles);
    }

    @Override
    public void onAttach(Context context) {
        
        super.onAttach(context);
        if (context instanceof CategoryGridAdapter.OnCategoryWithTitleClickListener) {
            onCategoryWithTitleClickListener = (CategoryGridAdapter.OnCategoryWithTitleClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentAListener");
        }
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
