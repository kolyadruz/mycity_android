package com.kolyadruz.mycity.ui.fragments.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.material.tabs.TabLayout;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.adapters.CategoryGridAdapter;
import com.kolyadruz.mycity.adapters.TopBannersAdapter;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.mvp.presenters.HomePagePresenter;
import com.kolyadruz.mycity.mvp.views.HomeView;
import com.kolyadruz.mycity.subnavigation.LocalCiceroneHolder;
import com.kolyadruz.mycity.ui.common.BackButtonListener;
import com.kolyadruz.mycity.ui.common.RouterProvider;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;


public class HomePage extends MvpAppCompatFragment implements HomeView, RouterProvider, BackButtonListener {

    private static final String TAG = "HomePage";

    private Navigator navigator;

    @Inject
    LocalCiceroneHolder ciceroneHolder;

    //TOP BANNERS
    @BindView(R.id.bannersViewPager)
    ViewPager bannersViewPager;
    @BindView(R.id.bannerDots)
    TabLayout bannerDots;
    private TopBannersAdapter bannersPagerAdapter;

    private Unbinder unbinder;

    private int pagesCount;

    private Timer time;
    private Integer page = 0;

    private Activity activity;

    private int categoryId = 0;

    @InjectPresenter
    HomePagePresenter presenter;

    @ProvidePresenter
    HomePagePresenter provideHomePagePresenter() {
        return new HomePagePresenter(getRouter());
    }

    public static HomePage getNewInstance(int categoryId) {

        HomePage fragment = new HomePage();

        Bundle args = new Bundle();
        args.putInt("categoryId", categoryId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getAppComponent().inject(this);

        if (getArguments() != null) {
            categoryId = getArguments().getInt("categoryId");
        }

        super.onCreate(savedInstanceState);
    }

    private Cicerone<Router> getCicerone() {
        return ciceroneHolder.getCicerone(TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        bannersPagerAdapter = new TopBannersAdapter(getChildFragmentManager());

        bannersViewPager.setAdapter(bannersPagerAdapter);
        bannersViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

                page = position + 1;

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        bannerDots.setupWithViewPager(bannersViewPager, true);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getChildFragmentManager().findFragmentById(R.id.category_container) == null) {
            getCicerone().getRouter().replaceScreen(new Screens.CategoriesTableScreen(categoryId));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getCicerone().getNavigatorHolder().setNavigator(getNavigator());

        ScheduleTimer();

    }

    private void ScheduleTimer() {

        if (time != null) {
            time.cancel();
            time = null;
        }

        TimerTask timer = new TimerTask() {
            @Override
            //enter code here
            public void run() {
                activity.runOnUiThread(viewPagerNextPage);
            }
        };
        time = new Timer();
        time.schedule(timer, 0, 8000);

    }

    private final Runnable viewPagerNextPage = new Runnable() {
        public void run() {

            if (bannersViewPager != null && page != null) {

                bannersViewPager.setCurrentItem(page);
                if (page < pagesCount) {
                    page++;
                } else {
                    page = 0;
                }

            }
        }
    };

    @Override
    public void onPause() {

        Log.d(TAG, "onPause: ");
        
        getCicerone().getNavigatorHolder().removeNavigator();

        super.onPause();
        if (time != null) {
            time.cancel();
            time = null;
        }
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.category_container);
        }
        return navigator;
    }

    @Override
    public void onDestroyView() {

        Log.d(TAG, "onDestroyView: ");
        
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public Router getRouter() {
        return getCicerone().getRouter();
    }

    @Override
    public void setBanners(List<PublicationDetail> banners) {

        bannersPagerAdapter.updateBanners(banners);

        pagesCount = banners.size();
        ScheduleTimer();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CategoryGridAdapter.OnCategoryWithTitleClickListener) {
            activity = (Activity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentAListener");
        }
    }


    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
