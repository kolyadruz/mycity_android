package com.kolyadruz.mycity.ui.fragments.home.banners;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Router;

public class Banner extends Fragment {

    @Inject
    Router router;

    private static final String IMAGE_URL = "image_url";
    private static final String POST_ID = "postId";

    private String imageUrl;
    private Integer postId;

    @BindView(R.id.banner)
    ImageButton imgButton;

    private Unbinder unbinder;

    public static Banner getNewInstance(String imageUrl, Integer postId) {
        Banner fragment = new Banner();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL, imageUrl);
        args.putInt(POST_ID, postId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        App.getAppComponent().inject(this);

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imageUrl = getArguments().getString(IMAGE_URL);
            postId = getArguments().getInt(POST_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.item_top_banner, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        Glide.with(App.getAppComponent().getContext()).load(imageUrl).into(imgButton);

        return rootView;
    }

    @OnClick(R.id.banner)
    void bannerCLicked() {

        Log.d("Banner", "Clicked postId: " + postId);
        router.navigateTo(new Screens.PostDetailScreen(postId));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
