package com.kolyadruz.mycity.ui.fragments.qrscanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.zxing.Result;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.mvp.NetworkService;
import com.kolyadruz.mycity.mvp.models.RequestBodies.ReadQrRequestBody;
import com.kolyadruz.mycity.mvp.models.ResponseModels.QR.QRResponseData;
import com.kolyadruz.mycity.utils.ErrorMessageUtils;
import com.welcu.android.zxingfragmentlib.BarCodeScannerFragment;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QrScanner extends BarCodeScannerFragment {

    private static final String TAG = "QrScanner";

    @Inject
    NetworkService networkService;

    AlertDialog.Builder ad;

    Activity activity;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.getAppComponent().inject(this);

        activity = getActivity();

        this.setmCallBack(new IResultCallback() {
            @Override
            public void result(Result lastResult) {

                stopScan();
                startDialog(lastResult.toString());

            }
        });
    }

    public QrScanner() {

    }

    @Override
    public void onResume() {
        super.onResume();
        startScan();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopScan();
    }

    public void startDialog(final String qr){
        final String[] actions ={"Отправить", "Отмена"};
        ad = new AlertDialog.Builder(activity);
        ad.setTitle("QR-код: "+qr);


        ad.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        ReadQrRequestBody body = new ReadQrRequestBody();
                        body.qrCode = qr;

                        networkService
                                .sendqr(body)
                                .enqueue(new Callback<QRResponseData>() {
                                    @Override
                                    public void onResponse(@NonNull Call<QRResponseData> call, @NonNull Response<QRResponseData> response) {

                                        Log.d(TAG, "onResponse: ");

                                        QRResponseData qrData = response.body();

                                        if (qrData.data.err_id == 0) {
                                            showSuccessDialog();
                                        } else {

                                            startScan();
                                            Toast.makeText(App.getAppComponent().getContext(), ErrorMessageUtils.getErrorMessage(qrData.data.err_id), Toast.LENGTH_LONG).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(@NonNull Call<QRResponseData> call, @NonNull Throwable t) {

                                        t.printStackTrace();

                                        startScan();
                                        Toast.makeText(App.getAppComponent().getContext(), t.toString(), Toast.LENGTH_LONG).show();

                                    }
                                });

                        break;
                    case 1:
                        startScan();
                        break;
                    default:
                        startScan();
                        break;
                }
            }
        });

        ad.show();
    }

    public void showSuccessDialog(){
        final String[] actions ={"ОК"};
        ad = new AlertDialog.Builder(activity);
        ad.setTitle("QR-код успешно прочитан.");
        ad.setMessage("Скидка может быть предоставлена.");
        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startScan();
            }
        });

        ad.show();
    }

}
