package com.kolyadruz.mycity.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;

import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.google.android.material.navigation.NavigationView;
import com.kolyadruz.mycity.Screens;
import com.kolyadruz.mycity.adapters.CategoryGridAdapter;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.ui.common.BackButtonListener;
import com.kolyadruz.mycity.ui.fragments.qrscanner.NoScanResultException;
import com.kolyadruz.mycity.ui.fragments.qrscanner.ScanResultReceiver;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.utils.IntentUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Screen;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;
import ru.terrakok.cicerone.commands.Replace;

public class MainActivity extends MvpAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ScanResultReceiver, CategoryGridAdapter.OnCategoryWithTitleClickListener {

    @Inject
    NavigatorHolder navigatorHolder;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.mainLogoImg)
    ImageView mainTitleImg;

    @BindView(R.id.titleTxt)
    TextView mainTitleTxt;

    @BindView(R.id.nav_view)
    NavigationView navView;

    @BindView(R.id.search_btn)
    ImageButton search_btn;

    Unbinder unbinder;

    ActionBarDrawerToggle toggle;

    private boolean mToolBarNavigationListenerIsRegistered = false;

    private boolean isFullMainTitleShown = false;

    public Navigator navigator = new SupportAppNavigator(this, R.id.page_container) {
        @Override
        public void applyCommands(Command[] commands) {
            super.applyCommands(commands);
            getSupportFragmentManager().executePendingTransactions();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        App.getAppComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);
        drawer.addDrawerListener(toggle);

        setNavigationHeaderButtons();

        if (savedInstanceState == null) {

            int categoryId;

            try {
                categoryId = getIntent().getExtras().getInt("categoryId", 0);
            } catch (Exception ex) {
                ex.printStackTrace();
                categoryId = 0;
            }

            navigator.applyCommands(new Command[]{new Replace(
                    new Screens.HomeScreen(categoryId)
                )
            });

            navView.setCheckedItem(R.id.nav_home);
        }

    }

    private void setNavigationHeaderButtons() {

        View headerView = navView.getHeaderView(0);

        headerView.findViewById(R.id.close_btn).setOnClickListener(
                v -> drawer.closeDrawer(GravityCompat.START)
        );

        headerView.findViewById(R.id.share_btn).setOnClickListener(
                v -> {

                    Intent sharingIntent = IntentUtils.getShareIntent(getShareBody());
                    startActivity(Intent.createChooser(sharingIntent, "Поделиться приложением"));
                }
        );

    }

    private String getShareBody() {

        String body = "Рекомендую скачать приложение Мой город - скидки, акции и события в нашем городе<br /><br />http://ggls.ru/app/download";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return  Html.fromHtml(body, Html.FROM_HTML_MODE_COMPACT).toString();
        } else {
            return HtmlCompat.fromHtml(body, HtmlCompat.FROM_HTML_MODE_LEGACY).toString();
        }

    }

    private void showNavigatorBackButton(boolean show) {

        if (show) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            if (!mToolBarNavigationListenerIsRegistered) {
                toggle.setToolbarNavigationClickListener(v -> onBackPressed());
                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {

            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }

            toggle.setDrawerIndicatorEnabled(true);
            toggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;

        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @OnClick(R.id.search_btn)
    void searchClicked() {
        isFullMainTitleShown = false;
        search_btn.setVisibility(View.GONE);
        navigator.applyCommands(new Command[]{new Forward(new Screens.SearchScreen())});
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.page_container);
        if (fragment instanceof BackButtonListener && ((BackButtonListener) fragment).onBackPressed()) {

            if (!isFullMainTitleShown) {
                showMainTitle();
                return;
            }

        }

        search_btn.setVisibility(View.VISIBLE);
        super.onBackPressed();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_home:
                switchPage("", new Screens.HomeScreen(0));
                break;
            case R.id.nav_favorites:
                switchPage("Избранное", new Screens.FavoritesScreen());
                break;
            case R.id.nav_settings:
                switchPage("Настройки", new Screens.SettingsScreen());
                break;
            case R.id.nav_collabaration:
                switchPage("Сотрудничество", new Screens.CollabarationScreen());
                break;
            case R.id.nav_qr:
                switchPage("Сканирование QR", new Screens.QrScreen());
                break;
            case R.id.nav_about:
                switchPage("О проекте", new Screens.AboutScreen());
                break;

        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void switchPage(String titleText, Screen screen) {

        mainTitleTxt.setText(titleText);
        mainTitleImg.setVisibility(titleText.equals("") ? View.VISIBLE : View.GONE);
        search_btn.setVisibility(titleText.equals("") ? View.VISIBLE : View.INVISIBLE);

        navigator.applyCommands(new Command[]{new Replace(screen)});
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public void showMainTitle() {

        isFullMainTitleShown = true;

        mainTitleTxt.setText("");
        mainTitleImg.setVisibility(View.VISIBLE);
        search_btn.setVisibility(View.VISIBLE);

        showNavigatorBackButton(false);

    }

    @Override
    public void scanResultData(String codeFormat, String codeContent) {
        Toast.makeText(App.getAppComponent().getContext(), "FORMAT: " + codeFormat + ", CONTENT: " + codeContent, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void scanResultData(NoScanResultException noScanData) {
        Toast.makeText(App.getAppComponent().getContext(),noScanData.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCategoryWithTitleClick(String title) {

        isFullMainTitleShown = false;
        
        mainTitleTxt.setText(title);
        mainTitleImg.setVisibility(View.GONE);
        search_btn.setVisibility(View.GONE);

        showNavigatorBackButton(true);

    }

}
