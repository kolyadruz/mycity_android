package com.kolyadruz.mycity.ui.fragments.search;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.mycity.adapters.CategoryGridAdapter;
import com.kolyadruz.mycity.adapters.PostsListAdapter;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.presenters.SearchPresenter;
import com.kolyadruz.mycity.mvp.views.SearchMvpView;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Router;

public class Search extends MvpAppCompatFragment implements PostsListAdapter.OnPostClickedListener, SearchMvpView {

    @Inject
    Router router;

    @InjectPresenter
    SearchPresenter presenter;

    @ProvidePresenter
    SearchPresenter provideSearchPresenter() {
        return new SearchPresenter(router);
    }

    @BindView(R.id.searchView)
    SearchView searchView;

    @BindView(R.id.recyclerView)
    RecyclerView postsList;
    PostsListAdapter recyclerAdapter;

    @BindView(R.id.empty_txt)
    TextView empty_txt;

    EditText searchViewEdiText;

    private Unbinder unbinder;

    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        empty_txt = rootView.findViewById(R.id.empty_txt);
        searchView = rootView.findViewById(R.id.searchView);

        return rootView;
    }

    @OnClick(R.id.cancel_button)
    void CancelPressed() {
        //presenter.onBackPressed();

        activity.onBackPressed();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        postsList.setLayoutManager(llm);

        recyclerAdapter = new PostsListAdapter( this);
        postsList.setAdapter(recyclerAdapter);

        searchViewEdiText = searchView.findViewById(androidx.appcompat.R.id.search_src_text);

        searchViewEdiText.setOnEditorActionListener((v, keyAction, keyEvent) -> {

            searchView.clearFocus();
            presenter.SearchRequest(searchViewEdiText.getText().toString());

            return false;

        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPostClicked(int position) {
        presenter.onPostClicked(position);
    }

    @Override
    public void updatePosts(List<PublicationDetail> posts) {
        recyclerAdapter.addData(posts);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof CategoryGridAdapter.OnCategoryWithTitleClickListener) {
            activity = (Activity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentAListener");
        }

    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
