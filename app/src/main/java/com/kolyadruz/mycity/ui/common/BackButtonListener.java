package com.kolyadruz.mycity.ui.common;

public interface BackButtonListener {

    boolean onBackPressed();

}
