package com.kolyadruz.mycity.ui.fragments.about;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.presenters.AboutPresenter;
import com.kolyadruz.mycity.mvp.views.AboutView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class About extends MvpAppCompatFragment implements AboutView {

    @InjectPresenter
    AboutPresenter presenter;

    @BindView(R.id.support_btn)
    Button supportEmailBtn;

    @BindView(R.id.version_tv)
    TextView version_tv;

    @BindView(R.id.supportTitle)
    TextView supportTitle_tv;

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.support_btn)
    void supportClicked() {
        presenter.onSupportPressed();
    }

    @OnClick(R.id.terms_btn)
    void termsClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ggls.ru/license"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.privacy_btn)
    void privacyClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ggls.ru/privacy"));
        startActivity(browserIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showInfo(String version, String emailSupport) {
        version_tv.setText(version);

        supportEmailBtn.setVisibility((emailSupport == null || emailSupport.equals(""))? View.GONE : View.VISIBLE);
        supportTitle_tv.setVisibility(supportEmailBtn.getVisibility());

        supportEmailBtn.setText(emailSupport);

    }

    @Override
    public void startIntent(Intent intent) {
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            showErrorID(8);
        }
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
