package com.kolyadruz.mycity.ui.fragments.collabaration;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.presenters.CollabarationPresenter;
import com.kolyadruz.mycity.mvp.views.CollabarationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Collabaration extends MvpAppCompatFragment implements CollabarationView {

    @InjectPresenter
    CollabarationPresenter presenter;

    @BindView(R.id.collabaration_info_with_phone_txt)
    TextView infoWithPhone;

    @BindView(R.id.collabaration_email_address)
    TextView email_tv;

    @BindView(R.id.collabaration_help_address)
    TextView emailTech_tv;

    @BindView(R.id.collabaration_support_title_txt)
    TextView supportTitleTV;

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_collabaration, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick(R.id.collabaration_email_address)
    void orderPressed() {
        presenter.onEmailPressed();
    }

    @OnClick(R.id.collabaration_help_address)
    void supportPressed() {
        presenter.onEmailTechPressed();
    }

    @OnClick(R.id.btn_call)
    void callPressed() {
        presenter.callPressed();
    }

    @OnClick(R.id.btn_insta)
    void instaPressed() {
        presenter.onInstaPressed();
    }

    @OnClick(R.id.btn_wa)
    void waPressed() {
        presenter.onWaPressed();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showInfo(String phoneNumber, String email, String emailTech) {
        infoWithPhone.setText("Чтобы разместить объявление, позвоните по номеру " + phoneNumber + " или напишите на Email");
        email_tv.setText(email);
        emailTech_tv.setText(emailTech);

        email_tv.setVisibility(email.equals("")? View.GONE : View.VISIBLE);

        emailTech_tv.setVisibility((emailTech == null || emailTech.equals(""))? View.GONE : View.VISIBLE);
        supportTitleTV.setVisibility(emailTech_tv.getVisibility());
    }

    @Override
    public void startIntent(Intent intent) {
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            showErrorID(8);
        }
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
