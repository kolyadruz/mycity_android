package com.kolyadruz.mycity.ui.fragments.favorites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.mycity.adapters.PostsListAdapter;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;
import com.kolyadruz.mycity.mvp.presenters.FavoritesPresenter;
import com.kolyadruz.mycity.mvp.views.FavoritesView;
import com.kolyadruz.mycity.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Router;


public class Favorites extends MvpAppCompatFragment implements PostsListAdapter.OnPostClickedListener, FavoritesView {

    @Inject
    Router router;

    @InjectPresenter
    FavoritesPresenter presenter;

    @ProvidePresenter
    FavoritesPresenter provideFavoritesPresenter() {
        return new FavoritesPresenter(router);
    }

    @BindView(R.id.recyclerView)
    RecyclerView favoritesRecycler;
    private PostsListAdapter favoritesAdapter;

    @BindView(R.id.empty_txt)
    TextView empty_txt;

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        LinearLayoutManager llm = new LinearLayoutManager(App.getAppComponent().getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        favoritesRecycler.setLayoutManager(llm);

        favoritesAdapter = new PostsListAdapter(this);
        favoritesRecycler.setAdapter(favoritesAdapter);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPostClicked(int position) {

        presenter.onPostCLicked(position);

    }

    @Override
    public void updatePosts(List<PublicationDetail> posts) {
        favoritesAdapter.addData(posts);
    }

    @Override
    public void showEmptyTxt() {
        empty_txt.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyTx() {
        empty_txt.setVisibility(View.GONE);
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
