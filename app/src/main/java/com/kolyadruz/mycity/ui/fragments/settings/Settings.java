package com.kolyadruz.mycity.ui.fragments.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.adapters.SettingsListAdapter;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.presenters.SettingsPresenter;
import com.kolyadruz.mycity.mvp.views.SettingsView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class Settings extends MvpAppCompatFragment implements OnSettingsSwitchTappedListener, SettingsView {

    @InjectPresenter
    SettingsPresenter presenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    SettingsListAdapter settingsAdapter;

    Unbinder unbinder;

    public Settings() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        settingsAdapter = new SettingsListAdapter(this);
        recyclerView.setAdapter(settingsAdapter);

        return rootView;
    }

    @Override
    public void onSettingsSwitchTapped(int position, final CompoundButton checkBox) {
        presenter.onSettingsSwitchTapped(position, checkBox);
    }

    @Override
    public void updateTopics(Topics subscriptions) {
        settingsAdapter.updateTopics(subscriptions);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
