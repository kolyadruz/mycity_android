package com.kolyadruz.mycity.ui.fragments.settings;

import android.widget.CompoundButton;

public interface OnSettingsSwitchTappedListener {

    public void onSettingsSwitchTapped(int position, CompoundButton mSwitch);

}
