package com.kolyadruz.mycity.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.firebase.FirebaseApp;

import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import permission.auron.com.marshmallowpermissionhelper.PermissionUtils;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;

import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.common.MvpActivityManagePermission;
import com.kolyadruz.mycity.mvp.presenters.AutoLoginPresenter;
import com.kolyadruz.mycity.mvp.views.AutoLoginView;

import javax.inject.Inject;


public class AutoLogin extends MvpActivityManagePermission implements AutoLoginView {

    @Inject
    Router router;

    @InjectPresenter
    AutoLoginPresenter autoLoginPresenter;

    @ProvidePresenter
    AutoLoginPresenter autoLoginPresenter() {
        return new AutoLoginPresenter(router);
    }

    @Inject
    NavigatorHolder navigatorHolder;

    public Navigator navigator = new SupportAppNavigator(this, R.id.page_container) {
        @Override
        public void applyCommands(Command[] commands) {
            super.applyCommands(commands);
            getSupportFragmentManager().executePendingTransactions();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        App.getAppComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autologin);

        FirebaseApp.initializeApp(getApplicationContext());

        askCompactPermissions(new String[]{
                PermissionUtils.Manifest_CALL_PHONE,
                PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE,
                PermissionUtils.Manifest_READ_EXTERNAL_STORAGE,
                PermissionUtils.Manifest_CAMERA
        }, new PermissionResult() {
            @Override
            public void permissionGranted() {
                autoLoginPresenter.permissionGranted();
            }

            @Override
            public void permissionDenied() {
                showDialog();
            }

            @Override
            public void permissionForeverDenied() {
                showDialog();
            }
        });

    }

    private void showDialog() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(AutoLogin.this);
        builder.setTitle("Внимание");
        builder.setMessage("Для корректной работы приложения требуются все необходимые разрешения. Пожалуйста, перейдите в настройки и предоставьте все запрашиваемые разрешения.");

        builder.setPositiveButton("Настройки", (DialogInterface dialogInterface, int i) -> openSettingsApp(this));
        builder.setNegativeButton("Отмена",  (DialogInterface dialogInterface, int i) -> this.finish());

        if (!isFinishing()) {
            builder.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getSupportFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
