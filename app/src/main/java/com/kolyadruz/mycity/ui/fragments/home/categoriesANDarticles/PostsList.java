package com.kolyadruz.mycity.ui.fragments.home.categoriesANDarticles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.mvp.presenters.PostsListPresenter;
import com.kolyadruz.mycity.mvp.views.PostsListView;
import com.kolyadruz.mycity.ui.common.RouterProvider;
import com.kolyadruz.mycity.adapters.PostsListAdapter;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.mvp.models.ResponseModels.PublicationDetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class PostsList extends MvpAppCompatFragment implements PostsListAdapter.OnPostClickedListener, PostsListView {

    private static final String TAG = "PostsList";

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.recyclerView)
    RecyclerView postsList;
    private PostsListAdapter recyclerAdapter;

    private Unbinder unbinder;

    @InjectPresenter
    PostsListPresenter presenter;

    @ProvidePresenter
    PostsListPresenter providePostsListPresenter() {
        return new PostsListPresenter(
                ((RouterProvider) getParentFragment()).getRouter(),
                getArguments().getInt("categoryId")
        );
    }

    public static PostsList getNewInstance(int categoryId) {

        PostsList fragment = new PostsList();

        Bundle args = new Bundle();
        args.putInt("categoryId", categoryId);
        fragment.setArguments(args);


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_posts_list, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        //POSTS LIST
        LinearLayoutManager llm = new LinearLayoutManager(App.getAppComponent().getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        postsList.setLayoutManager(llm);

        recyclerAdapter = new PostsListAdapter(this);
        postsList.setAdapter(recyclerAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPostClicked(int position) {
        presenter.onPostClicked(position);
    }

    @Override
    public void updateList(List<PublicationDetail> fixed, List<PublicationDetail> posts) {
        recyclerAdapter.updateList(fixed, posts);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
