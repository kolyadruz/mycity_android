package com.kolyadruz.mycity.ui.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.DialogFragment;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kolyadruz.mycity.R;
import com.kolyadruz.mycity.app.App;
import com.kolyadruz.mycity.dialogs.ErrIDMessageDialog;
import com.kolyadruz.mycity.dialogs.QRdialog;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.Phone;
import com.kolyadruz.mycity.mvp.models.ResponseModels.Posts.PublicationDetailExtra;
import com.kolyadruz.mycity.mvp.presenters.PostDetailPresenter;
import com.kolyadruz.mycity.mvp.views.PostDetailView;
import com.vansuita.gaussianblur.GaussianBlur;

import net.glxn.qrgen.android.QRCode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;


public class PostDetail extends MvpAppCompatActivity implements PostDetailView {

    @Inject
    Router router;

    @Inject
    NavigatorHolder navigatorHolder;

    private Navigator navigator = new SupportAppNavigator(this, -1);

    @InjectPresenter
    PostDetailPresenter presenter;

    @ProvidePresenter
    PostDetailPresenter providePostDetailPresenter() {
        return new PostDetailPresenter(router, getIntent().getIntExtra("postId", 0));
    }

    AlertDialog.Builder phoneNumbersAlert;

    @BindView(R.id.like_btn)
    ImageButton likeBtn;

    @BindView(R.id.post_image)
    ImageView image;
    @BindView(R.id.post_blur_image)
    ImageView blurImage;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.viewsTxt)
    TextView views;

    @BindView(R.id.description)
    TextView descriptionTxt;

    @BindView(R.id.qr_cell)
    RelativeLayout qrCell;

    @BindView(R.id.insta_btn)
    ImageButton instaBtn;

    @BindView(R.id.call_btn)
    FrameLayout callBtn;
    @BindView(R.id.call_btn_title)
    TextView callBtnTitle;
    @BindView(R.id.call_btn_icon)
    ImageView callBtnIcon;

    @BindView(R.id.write_btn)
    FrameLayout writeBtn;
    @BindView(R.id.write_btn_title)
    TextView writeBtnTitle;
    @BindView(R.id.write_btn_icon)
    ImageView writeBtnIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        App.getAppComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        ButterKnife.bind(this);

    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @OnClick(R.id.qrButton)
    void qrTapped() {
        presenter.qrButtonTapped();
    }

    @OnClick(R.id.insta_btn)
    public void instaTapped() {
        presenter.onInstaPressed();
    }

    @OnClick(R.id.call_btn)
    public void callTapped() {
        phoneNumbersAlert.show();
    }

    @OnClick(R.id.write_btn)
    public void waTapped() {
        presenter.onWaPressed();
    }

    @OnClick(R.id.close_btn)
    public void closeTapped() {
        presenter.onClosePressed();
    }

    @OnClick(R.id.like_btn)
    public void likeTapped() {
        presenter.onLikeTapped();
    }

    @OnClick(R.id.wa_share_btn)
    public void waShareTapped() {
        presenter.waSharePressed();
    }

    @OnClick(R.id.other_share_button)
    public void otherShareTapped() {
        presenter.otherSharePressed();
    }

    @Override
    public void onBackPressed() {
        presenter.onClosePressed();
    }

    @Override
    public void showInfo(PublicationDetailExtra post) {

        Glide.with(App.getAppComponent().getContext())
                .load(post.image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        Bitmap blurredBitmap = GaussianBlur.with(App.getAppComponent().getContext()).size(200).radius(25).render(resource);
                        blurImage.setImageBitmap(blurredBitmap);

                        return false;
                    }
                })
                .into(image);

        if (post.instagram == null) {
            instaBtn.setEnabled(false);
            instaBtn.setImageDrawable(ContextCompat.getDrawable(App.getAppComponent().getContext(), R.drawable.ic_share_insta_bw));
        }

        if (post.phonesCall.size() == 0) {
            callBtn.setEnabled(false);
            callBtnTitle.setTextColor(ContextCompat.getColor(this, R.color.light_gray));
            callBtnIcon.setColorFilter(ContextCompat.getColor(this, R.color.light_gray));
        }

        if (post.phoneWa == null) {
            writeBtn.setEnabled(false);
            writeBtnTitle.setTextColor(ContextCompat.getColor(this, R.color.light_gray));
            writeBtnIcon.setColorFilter(ContextCompat.getColor(this, R.color.light_gray));
        }

        if (post.isFavorite) {
            likeBtn.setImageResource(R.drawable.ic_heart_active);
        }

        title.setText(post.title);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            descriptionTxt.setText(Html.fromHtml(post.body, Html.FROM_HTML_MODE_COMPACT));
        } else {
            descriptionTxt.setText(HtmlCompat.fromHtml(post.body, HtmlCompat.FROM_HTML_MODE_LEGACY));
        }

        views.setText(String.valueOf(post.views));

        qrCell.setVisibility(post.qrData == null? View.GONE : View.VISIBLE);

    }

    @Override
    public void buildAlert(List<Phone> phoneNumbers) {

        List<String> actions = new ArrayList<>();

        for (Phone phone: phoneNumbers) {
            actions.add(phone.description.equals("")? phone.phone : phone.description);
        }

        final String[] array = actions.toArray(new String[phoneNumbers.size()]);

        phoneNumbersAlert = new AlertDialog.Builder(this);
        phoneNumbersAlert.setTitle("Выберите номер для звонка");

        phoneNumbersAlert.setItems(array, (DialogInterface dialog, int position) -> {

            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},1);
            } else {
                presenter.onCallPressed(position);
            }

        });

    }

    @Override
    public void setLiked(boolean isFavorite) {
        likeBtn.setImageResource( isFavorite ? R.drawable.ic_heart_active : R.drawable.ic_heart);
    }

    @Override
    public void startIntent(Intent intent) {
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            showErrorID(8);
        }
    }

    @Override
    public void showQRdialog(String title, String url) {
        DialogFragment dialogFragment = QRdialog.getNewInstance(title, url);
        dialogFragment.show(getSupportFragmentManager(), dialogFragment.getClass().getName());
    }

    @Override
    public void showErrorID(int errID) {
        DialogFragment dialog = ErrIDMessageDialog.getNewInstance(errID);
        dialog.show(getSupportFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void showConnectionErrorWithRequestCode(int requestCode) {

    }
}
